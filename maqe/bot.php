<?php
namespace Maqe;

class Bot 
{
	public $command;

	private $_command_list;
	private $_digit;

	public $position;
	public $direction;

	public function __construct()
	{
		$this->direction = 1; // 1=N,2=E,3=S,4=W
		$this->position = array(
			'x' => 0,
			'y' => 0
		);
	}

	private function throwError($text)
	{
		echo $text;
		die();
	}

	public function create($command)
	{
		$this->command = $command;
		// echo 'Command is: ' . $this->command . "\n<br>";
		$this->_command_list = str_split($this->command);
		// echo "<pre>";
		// print_r($this->_command_list);
	}

	public function checkFormat()
	{
		// L,R,W, Digit checking
		if(!preg_match_all('/^[LRW0-9]+$/', $this->command, $matches))
		{
			return false;
		}

		if(substr($this->command, -1) == 'W')
		{
			return false;
		}

		// W+n checking
		preg_match_all('/\d+/', $this->command, $matches, PREG_OFFSET_CAPTURE);
		foreach($matches[0] as $key => $obj)
		{
			$prevKey = $obj[1]-1;
			if($prevKey < 0 || $this->_command_list[$prevKey] != 'W')
			{
				return false;
			}
			$this->_digit = $matches[0];
		}

		preg_match_all('/W{1}+./', $this->command, $matches, PREG_OFFSET_CAPTURE);
		foreach($matches[0] as $key => $obj)
		{
			$wN = $obj[0];
			$last = substr($wN, -1);
			if($last == 0 || !is_int((int)$last))
			{
				return false;
			}

		}
		return true;
	}

	private function _callDirection($char)
	{
		$turn = ($char == 'L')?-1:1;
		$newDirection = $this->direction;
		$newDirection+= $turn;
		if($newDirection > 4)
		{
			$newDirection = 1;
		}else if($newDirection < 1){
			$newDirection = 4;
		}
		$this->direction = $newDirection;
		// echo '::direction: ' . $this->direction . '<hr>';
	}

	private function _walk($step) 
	{
		// echo "step: " . $step . "<br>";
		switch ($this->direction) {
			case 1:
				$this->position['y']+= ($step*1);
				break;
			
			case 2:
				$this->position['x']+= ($step*1);
				break;
			
			case 3:
				$this->position['y']+= ($step*-1);
				break;
			
			case 4:
				$this->position['x']+= ($step*-1);
				break;
		}
	}

	public function doit()
	{
		$max = strlen($this->command);
		// echo "<pre>";
		// print_r($this->_digit);
		// print_r($this->_command_list);
		for($i = 0; $i<$max; $i++)
		{
			$char = $this->_command_list[$i];
			// echo "i: $i , t: " . $char . '<br>';
			if($char == 'L' || $char == 'R')
			{
				$this->_callDirection($char);
			}else if($char == 'W') {
				$step = (int)$this->_digit[0][0];
				$this->_walk($step);
				$i+= strlen($step);
				array_shift($this->_digit);
			}
		}

	}

	public function directionToString()
	{
		switch ($this->direction) {
			case 1:
				return 'North';
				break;
			
			case 2:
				return 'East';
				break;
			
			case 3:
				return 'South';
				break;
			
			case 4:
				return 'West';
				break;
		}
	}

	public function answer() 
	{
		$format = 'X: %d Y: %d Direction: %s';
		return sprintf($format, $this->position['x'], $this->position['y'], $this->directionToString());
	}

}
?>